#/usr/bin/python
#-*- coding: utf-8 -*-
import sys,time
import matplotlib
matplotlib.use('Agg') #Enable this when running from Cluster account. This disables matplotlib.show() command.
import numpy, IPython
import matplotlib.pyplot as plt
from matplotlib import rc
from scipy import spatial
import TrackGalaxy
import astropy.units as u
import astropy.constants as const
import MaanIonisationTables.mhh_ion_tables as mhh_ion_tables
import Spectra
import Misc.Papaya


###############################################################
###################### Reading in snapshot ####################
###############################################################

h=0.7
SimName = 'Hestia17-11'
SnapNo = 127
RMAX = 1400.0#An arbitrary number

if SimName == 'Hestia17-11':
    SubhaloNumberMW = 1#These numbers come from cross-correlating with /z/nil/codes/HESTIA/FIND_LG/LGs_8192_GAL_FOR.txt andArepo's SUBFIND.
    SubhaloNumberM31 = 0
    SimulationDirectory = '/store/clues/HESTIA/RE_SIMS/8192/GAL_FOR/17_11/output_2x2.5Mpc/'
elif SimName == 'Hestia09-18':
    SubhaloNumberMW = 3911
    SubhaloNumberM31 = 2608
    SimulationDirectory = '/store/clues/HESTIA/RE_SIMS/8192/GAL_FOR/09_18/output_2x2.5Mpc/'
elif SimName == 'Hestia37-11':
    SubhaloNumberMW = 920
    SubhaloNumberM31 = 0
    SimulationDirectory = '/store/clues/HESTIA/RE_SIMS/8192/GAL_FOR/37_11/output_2x2.5Mpc/'
else:
    print( 'SimName',SimName,'is not properly set, try again!. exiting.')
    sys.exit()
    

print( 'Initialising TrackGalaxy class')
T = TrackGalaxy.TrackGalaxy( numpy.arange(127,40,-1)  ,SimName,Dir = SimulationDirectory,MultipleSnaps=True) #Imports TrackGalaxy module from TrackGalaxy script
T.DefineSubhaloToTrack(SubhaloNumberMW,127,DownSampleN=10000)

#T.TrackProgenitor(PickleName = T.SimName + '_0UpdateAtEachStepiHardcode.pickle',SubhaloAttrsToSave = ['Subhalo/SubhaloHalfmassRadType','Subhalo/SubhaloSFR','Subhalo/SubhaloBHMass','Subhalo/SubhaloMassType','Subhalo/SubhaloPos','Subhalo/SubhaloVel','Group/Group_R_Crit200'],UpdateIDsAtEachStep = True,CreateImage=False,HardCodedSnaps=[121],HardCodedSubhalos=[2910],HardCodedGroups=[2])
T.TrackProgenitor(PickleName = T.SimName + '_UpdateAtEachStep.pickle',SubhaloAttrsToSave = ['Subhalo/SubhaloHalfmassRadType','Subhalo/SubhaloSFR','Subhalo/SubhaloBHMass','Subhalo/SubhaloMassType','Subhalo/SubhaloPos','Subhalo/SubhaloVel','Group/Group_R_Crit200'],UpdateIDsAtEachStep = True,CreateImage=False)

#T = TrackGalaxy.TrackGalaxy( numpy.arange(127,40,-1)  ,SimName,Dir = SimulationDirectory,MultipleSnaps=True) #Imports TrackGalaxy module from TrackGalaxy script
#T.DefineSubhaloToTrack(SubhaloNumberMW,127,DownSampleN=10000)
#T.TrackProgenitor(PickleName = T.SimName + '_0.pickle',SubhaloAttrsToSave = ['Subhalo/SubhaloHalfmassRadType','Subhalo/SubhaloSFR','Subhalo/SubhaloBHMass','Subhalo/SubhaloMassType','Subhalo/SubhaloPos','Subhalo/SubhaloVel','Group/Group_R_Crit200'],UpdateIDsAtEachStep = False,CreateImage=False)
#print('All stuff saved',T.GalaxyProp.keys())

for i in range(len(T.Snapshots)):
    print(T.Snapshots[i],T.GroupNumberOfTrackedHalo[i],T.SubhaloNumberOfTrackedHalo[i],T.GalaxyProp['Subhalo/SubhaloPos'][i],T.GalaxyProp['Subhalo/SubhaloSFR'][i])


    plt.figure(11)
    plt.clf()
    plt.subplot(1,1,1)
    ax = plt.gca()
    #IPython.embed()
    AttrsSubhalo = T.GetSubhaloParticles(T.Snapshots[i],T.SubhaloNumberOfTrackedHalo[i], Type=4, Attrs=['Coordinates', 'ParticleIDs', 'GFM_StellarFormationTime','Masses'])
    Attrs = T.GetParticles(T.Snapshots[i], Type=4, Attrs=['Coordinates', 'ParticleIDs', 'GFM_StellarFormationTime','Masses'])

    BoxHalfWidth = 0.200

    Pos = Attrs['Coordinates']
    Masses = Attrs['Masses']
    FormTime = Attrs['GFM_StellarFormationTime']
    GoodIDs = numpy.where(  
            (Pos[:,0]>T.GalaxyProp['Subhalo/SubhaloPos'][i][0]-BoxHalfWidth ) * (Pos[:,0]<T.GalaxyProp['Subhalo/SubhaloPos'][i][0]+BoxHalfWidth ) *
            (Pos[:,1]>T.GalaxyProp['Subhalo/SubhaloPos'][i][1]-BoxHalfWidth ) * (Pos[:,1]<T.GalaxyProp['Subhalo/SubhaloPos'][i][1]+BoxHalfWidth ) *
            (Pos[:,2]>T.GalaxyProp['Subhalo/SubhaloPos'][i][2]-BoxHalfWidth ) * (Pos[:,2]<T.GalaxyProp['Subhalo/SubhaloPos'][i][2]+BoxHalfWidth ) *
            (FormTime>0.0)
            )
    Pos = Pos[GoodIDs]
    Masses = Masses[GoodIDs]
    FormTime = FormTime[GoodIDs]
    AreaPerPixel = (1000*BoxHalfWidth*2.0/150)**2#in kpc**2
    plt.hist2d(Pos[:,0],Pos[:,1],bins=150, norm='log', range=[[T.GalaxyProp['Subhalo/SubhaloPos'][i][0]-BoxHalfWidth,T.GalaxyProp['Subhalo/SubhaloPos'][i][0]+BoxHalfWidth], [T.GalaxyProp['Subhalo/SubhaloPos'][i][1]-BoxHalfWidth,T.GalaxyProp['Subhalo/SubhaloPos'][i][1]+BoxHalfWidth]],cmap='RdBu_r',weights=Masses*1e10/0.67/AreaPerPixel,vmax=1e9,vmin=1e1)
    plt.colorbar(label=r'$\Sigma_\star$ [M$_\odot$ kpc$^{-2}$]')
    plt.plot(T.GalaxyProp['Subhalo/SubhaloPos'][i][0],T.GalaxyProp['Subhalo/SubhaloPos'][i][1],'x',ms=12,mew=2,color='black',zorder=7)

    GroupCatalog = T.GetGroups(T.Snapshots[i],Attrs=['Subhalo/SubhaloMassType','Subhalo/SubhaloPos'])
    SubhaloPos = GroupCatalog['Subhalo/SubhaloPos'] 
    SubhaloMstar = GroupCatalog['Subhalo/SubhaloMassType'][:,4]*1e10/0.67
    SubhaloNumber = numpy.arange(SubhaloMstar.size)
    GoodIDs = numpy.where(  
            (SubhaloPos[:,0]>T.GalaxyProp['Subhalo/SubhaloPos'][i][0]-BoxHalfWidth ) * (SubhaloPos[:,0]<T.GalaxyProp['Subhalo/SubhaloPos'][i][0]+BoxHalfWidth ) *
            (SubhaloPos[:,1]>T.GalaxyProp['Subhalo/SubhaloPos'][i][1]-BoxHalfWidth ) * (SubhaloPos[:,1]<T.GalaxyProp['Subhalo/SubhaloPos'][i][1]+BoxHalfWidth ) *
            (SubhaloPos[:,2]>T.GalaxyProp['Subhalo/SubhaloPos'][i][2]-BoxHalfWidth ) * (SubhaloPos[:,2]<T.GalaxyProp['Subhalo/SubhaloPos'][i][2]+BoxHalfWidth ) *
            (SubhaloMstar>5e7)
            )

    SubhaloPos = SubhaloPos[GoodIDs]
    SubhaloMstar = SubhaloMstar[GoodIDs]
    SubhaloNumber = SubhaloNumber[GoodIDs]

    for j in range(len(SubhaloNumber)):
        print(j)
        #IPython.embed()
        plt.plot(SubhaloPos[j][0],SubhaloPos[j][1],'x',ms=7,mew=1,color='cyan',zorder=8)
        plt.text(SubhaloPos[j][0],SubhaloPos[j][1],'(%d,%2.2f)'%(SubhaloNumber[j], numpy.log10(SubhaloMstar[j])),zorder=10,fontsize=7)

    plt.title('Snapshot: %d. $a=$%2.2f. $z=%2.2f$'%(T.Snapshots[i],T.SnapTimes[i],1/T.SnapTimes[i]-1))
    ax.set_aspect('equal', adjustable='box')


    plt.savefig('plots/'+T.SimName+'_'+str(i).zfill(3)+'.pdf')
    plt.savefig('plots/'+T.SimName+'_'+str(i).zfill(3)+'.jpg',dpi=300)


print(T.SimName)
print('SubhaloNumber = {}')
for i in range(len(T.Snapshots)):
    print('SubhaloNumber[%d]=%d '%(T.Snapshots[i],T.SubhaloNumberOfTrackedHalo[i]))
#IPython.embed()
